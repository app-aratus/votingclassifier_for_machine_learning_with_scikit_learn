import pandas
from sklearn import model_selection
from sklearn.ensemble import VotingClassifier
from imblearn.ensemble import EasyEnsembleClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier

import warnings
from sklearn.exceptions import ConvergenceWarning

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=ConvergenceWarning)


aline = "-------------------------------------------------------------------------------"

def describeDataFrame(dataframe):
    print( " " )
    print( aline )
    print( "dataframe.shape[1] col count")
    print(  dataframe.shape[1] )
    print( "dataframe.shape[0] row count")
    print(  dataframe.shape[0] )
    print( aline )
    print( "dataframe.head(5)")
    print(  dataframe.head(5))
    print( aline )
    print( "dataframe.describe()")
    print(  dataframe.describe())
    print( aline )

def main():

    # Input file
    csvFile      = 'pima-indians-diabetes-data.csv'

    # Pandas dataframe
    dataframe    = pandas.read_csv( csvFile, delimiter = ',') 

    # Describe the dataframe
    describeDataFrame(dataframe)

    # Dataframe values
    array        = dataframe.values

    # Features
    X            = array[:,0:8]

    # Response vector
    Y            = array[:,8]

    # Set a random state so we get the same accuracy every time we run.
    randomstate  = 7

    # Set 10 folds
    kfold = model_selection.KFold(n_splits=10, random_state=randomstate)

    # Define a classifier / model
    a = EasyEnsembleClassifier(n_estimators=10)
    b = BaggingClassifier()
    c = KNeighborsClassifier()

    estimators = []
    estimators.append(('a',      a)) 
    estimators.append(('b',      b))
    estimators.append(('c',      c))

    model = VotingClassifier(estimators) 

    # Get the cross validation score
    results = model_selection.cross_val_score(model, X, Y, cv=kfold)

    # Get the mean of the cross validation score
    print("The following is the mean of the cross validation score")
    print(results.mean())
    print(aline)

    # Returns
    # 0.7524094326725905


# This where the Python script starts
if __name__ == '__main__':
    main()
